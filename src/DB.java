
import java.sql.*;
import java.util.Vector;

public class DB {

    private static String username = "root";
    private static String password = "jacomus";
    private static String dbname = "arebookings";

    private static Connection connect = null;
    private static Statement statement = null;
    private static PreparedStatement preparedStatement = null;
    private static ResultSet resultSet = null;

    public static boolean connectDB() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager
                    .getConnection("jdbc:mysql://localhost/" + dbname + "?" + "user=" + username + "&password=" + password+ "&useSSL=false");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /*
    * @Param token = generated token
    * @Param uid = unique user id
    * @Param type = token type -/ possible values | register | reset
    * */
    public static void insertToken(String token, int uid, String type) {
        try {
            connectDB();

            String query = "INSERT INTO tokens (token,uid,type) VALUES (?,?,?)";
            PreparedStatement p = connect.prepareStatement(query);
            p.setString(1, token);
            p.setInt(2, uid);
            p.setString(3, type);
            p.executeUpdate();

            connect.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean insertMovie(String movieName, int cid, String desc) {
        try {
            connectDB();
            preparedStatement =
                    connect.prepareStatement
                        ("INSERT INTO `movies`\n" +
                        "(\n" +
                        "`movie_name`,\n" +
                        "`cid`,\n" +
                        "`description`)\n" +
                        "VALUES\n" +
                        "(\n" +
                        "?,\n" +
                        "?,\n" +
                        "?\n" +
                        ");");
            preparedStatement.setString(1, movieName);
            preparedStatement.setInt(2, cid);
            preparedStatement.setString(3, desc);

            preparedStatement.executeUpdate();
            connect.close();
            return true;
        } catch (SQLException e) {
            Utilities.conOut(e.getMessage());
            return false;
        }
    }
    
    public static String updateMovie(int ID, String movieName, int cid, String desc){
        try{
            connectDB();
            preparedStatement  = 
                    connect.prepareStatement
                    ("UPDATE `movies`\n" +
                    "SET\n" +
                    "`movie_name` = ?,\n" +
                    "`cid` = ?,\n" +
                    "`description` = ?\n" +
                    "WHERE `id` = ?;");
            
          preparedStatement.setString(1, movieName);
          preparedStatement.setInt(2, cid);
          preparedStatement.setString(3, desc);
          preparedStatement.setInt(4, ID);
          
          int checkUpdate = preparedStatement.executeUpdate();
          if(checkUpdate == 1){
              return "Record updated successfully!";
          }
            
        }catch(SQLException e)
        {
            e.printStackTrace();
        }
        return "";
    }
    
    public static String deleteMovie(int ID){
        try{
            connectDB();
            preparedStatement = connect.prepareStatement("DELETE FROM movies WHERE id = ?");
            preparedStatement.setInt(1,ID);
            
            int checkDelete = preparedStatement.executeUpdate();
            
            if(checkDelete == 1){
                return "Record deleted successfully!";
            }
            
        }catch(SQLException e){
            e.printStackTrace();
        }
        return "Exception occured!";
    }
    


    /*Checks password credentials, if user found returns true
    * @Param username
    * @Param password | password gets md5 hashed within function | method
    * */
    public static String checkLogin(String username, String password) throws SQLException {
        try {
            connectDB();
            preparedStatement = connect.prepareStatement("SELECT * FROM users WHERE username = ? AND password = ? AND status = ?");
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            preparedStatement.setString(3, "active");

            resultSet = preparedStatement.executeQuery();
            int rid = 0;
            int uid = 0;
            while (resultSet.next()) {
                rid = resultSet.getInt(6);
                uid = resultSet.getInt(1);
                connect.close();
                return rid + "-" + uid;
            }
            connect.close();
            return "";
        } catch (SQLException e) {
            Utilities.conOut(e.getMessage());
            return "";
        } finally {
            connect.close();
        }
    }
    
    public static String insertTheatre(int branchId, String theatreName, int capacity){
        
        try
        {
            connectDB();
            preparedStatement =
                    connect.prepareStatement
                        ("INSERT INTO `arebookings`.`theatre`\n" +
                        "(\n" +
                        "`theatrename`,\n" +
                        "`branchid`,\n" +
                        "`seatcapacity`)\n" +
                        "VALUES\n" +
                        "(\n" +
                        "?,\n" +
                        "?,\n" +
                        "?);");
            preparedStatement.setString(1, theatreName);
            preparedStatement.setInt(2, branchId);
            preparedStatement.setInt(3, capacity);
            
            int checkExec = preparedStatement.executeUpdate();
            if(checkExec == 1){
                return "Theatre inserted successfully!";
            }
            
        }catch(SQLException e)
        {
            e.printStackTrace();
        }
        return "";
    }
    
    public static String deleteTheatre(int id){
        try{
            connectDB();
            preparedStatement = connect.prepareStatement("DELETE FROM theatre WHERE id = ?");
            preparedStatement.setInt(1, id);
            
            int checkDelete = preparedStatement.executeUpdate();
            
            if(checkDelete == 1){
                return "Record deleted successfully!";    
            }
            
        }catch(SQLException e){
            e.printStackTrace();
        }
        return "";
    }
    
    public static String updateTheatre(int ID, String theatreName, int branchid, int capacity){
        
       try{
           connectDB();
           preparedStatement =
                   connect.prepareStatement
                    ("UPDATE `theatre`\n" +
                    "SET\n" +
                    "`theatrename` = ?,\n" +
                    "`branchid` = ?,\n" +
                    "`seatcapacity` = ?\n" +
                    "\n" +
                    "WHERE `id` = ?;");
           
          preparedStatement.setString(1, theatreName);
          preparedStatement.setInt(2,branchid);
          preparedStatement.setInt(3, capacity);          
          preparedStatement.setInt(4, ID);          
           
           int checkUpdate = preparedStatement.executeUpdate();
           if(checkUpdate == 1){
               return "Record updated successfully!";
           }
       }catch(SQLException e){
           e.printStackTrace();
       }
       return "";
    }
    
    public static String updateUser(int uid, String username, String name, String surname, String password, String eMail, String address, int age, String gender, String status ){
        try
        {
            connectDB();
            preparedStatement = 
                    connect.prepareStatement
                        ("UPDATE `users`\n" +
                        "SET\n" +
                        "`username` = ?,\n" +
                        "`name` = ?,\n" +
                        "`surname` = ?,\n" +
                        "`email` = ?,\n" +
                        "`adress` = ?,\n" +
                        "`age` = ?,\n" +
                        "`gender` = ?,\n" +
                        "`status` = ?\n" +
                        "WHERE `uid` = ?;");
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, name);
            preparedStatement.setString(3, surname);
            preparedStatement.setString(4, eMail);
            preparedStatement.setString(5, address);
            preparedStatement.setInt(6, age);
            preparedStatement.setString(7,gender);
            preparedStatement.setString(8, status);
            preparedStatement.setInt(9, uid);
            if(preparedStatement.executeUpdate() == 1){
                if(!password.equals("")){
                    preparedStatement = connect.prepareStatement("UPDATE users SET `password` = ? WHERE uid = ?");    
                    preparedStatement.setString(1,password);
                    preparedStatement.setInt(2, uid);
                    preparedStatement.executeUpdate();
                    return "Records updated successfully!";
                }
                return "Records updated successfully!";
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return "";
    }
    /*Checks the validity of token | token expiration time is 10 minutes
    * @Param token provided by user
    * @Param type = token type -/ possible values | register | passreset | Should be supplied internally for password reset and register
    * @Param uid = unique identifier of the user
    * */
    public static String checkToken(String token, String type, int uid) {
        try {
            connectDB();
            statement = connect.createStatement();
            resultSet = statement.executeQuery("SELECT *,"
                    + "IF(MINUTE(TIMEDIFF(NOW(), tokentime)) <= 60, 1, 0) AS isValid "
                    + " FROM tokens WHERE token = '" + token + "' AND type = '" + type + "' AND uid = " + uid + ";");//token expires in 10 minutes
            
            String returnMessage = "";
            while (resultSet.next()) {
                Utilities.conOut("Token found with value=" + token);
                int isValid = resultSet.getInt(6);
                if ("register".equals(type) && 1 == isValid) {
                    Utilities.conOut("UPDATING status");
                    updateUserStatus(uid, "active");
                    returnMessage = "Your account has been successfully activated!";
                } else if (isValid == 0) {
                    connect.close();
                    returnMessage ="Token expired, please contact us via e-mail at aretheatre@gmail.com!.";
                }
                connect.close();
                if("register".equals(type)){
                    return returnMessage;
                }else{
                    return "Your password has been succesfully renewed!";
                }
            }
        } catch (SQLException e) {
            Utilities.conOut("" + e.getMessage());
            return "";
        }
        return "";
    }


    /*Updates user status after token check
    *@Param uid = unique user ID
    *@Param status = possible values | active | pending | disabled
    * */
    public static boolean updateUserStatus(int uid, String status) {
        try {
            connectDB();
            preparedStatement = connect.prepareStatement("UPDATE users SET status = ? WHERE uid = ?");
            preparedStatement.setString(1, status);
            preparedStatement.setInt(2, uid);
            preparedStatement.executeUpdate();
            Utilities.conOut("USER STATUS UPDATED SUCCESFULLY TO =" + status);
            connect.close();
            return true;
        } catch (SQLException e) {
            Utilities.conOut("" + e.getMessage());
            return false;
        }
    }

//    @Nullable
    /*Lists users depending on role id
    * @Param rid = role ID | possible values 1 = admin, 2 = member
    * */
    public static Vector listUsers(int rid) {
        try {
            connectDB();
            statement = connect.createStatement();
            String execQ = "SELECT `uid`,`name`, `surname`,`username`,`email`,`adress`,`age`,`gender`, `status` FROM `users` WHERE 1=1 ";
            resultSet = statement.executeQuery(execQ);
            if(rid != 0){
                execQ += " rid = " + rid;
            }
            
            Vector resultV = new Vector();
            while (resultSet.next()) {
                Vector v = new Vector();
                v.add(resultSet.getInt(1));
                v.add(resultSet.getString(2));
                v.add(resultSet.getString(3));
                v.add(resultSet.getString(4));
                v.add(resultSet.getString(5));
                v.add(resultSet.getString(6));
                v.add(resultSet.getString(7));
                v.add(resultSet.getString(8));
                v.add(resultSet.getString(9));
                resultV.add(v);
            }
            Utilities.conOut("Listing users successfully");
            connect.close();
            return resultV;
        } catch (SQLException e) {
            Utilities.conOut(e.getMessage());
            return null;
        }
    }

    /*
    * Lists theatres
    * */
//    @Nullable
    public static Vector listMovies(int catid, boolean wombo) {
        try {
            connectDB();
            statement = connect.createStatement();
            String execQ = "SELECT movies.id,movie_name, category_name, description FROM arebookings.movies\n"
                    + "INNER JOIN categories ON categories.id = movies.cid ";

            if (catid != 0) {
                execQ += " WHERE cid = " + catid;
            }
            resultSet = statement.executeQuery(execQ);
            Vector movies = new Vector();

            while (resultSet.next()) {
                Vector v = new Vector();
                if(wombo == false){
                v.add(resultSet.getInt(1));
                v.add(resultSet.getString(2));
                v.add(resultSet.getString(3));
                v.add(resultSet.getString(4));
                }
                else{
                    v.add(resultSet.getString(2));
                }
                movies.add(v);
            }
            return movies;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    /*Gets the movie sessions | beware that session_times will probably return a JSON Object and needs to be parsed
    * */
    public static Vector listAvailableSessions(int theatreId, int movieId, int categoryId) {
        try {
            connectDB();

            Vector resultV = new Vector();

            //todo reassess the returning values.
            String selectStatement = "SELECT "
                    + "sessions.id,"
                    + "branch.branchname,"
                    + "theatre.theatrename,"
                    + "movies.movie_name,"
                    + "CONCAT(schedule_times.start , '-',schedule_times.end) AS sessionHours,"
                    + "theatre.seatcapacity - (SELECT IFNULL(sum(seatcount),0) FROM bookings WHERE bookings.session_id = sessions.id AND status = 'CONFIRMED') AS remainingSeatCount,"
                    + "description "
                    + "FROM sessions "
                    + "INNER JOIN movies ON sessions.movieid = movies.id\n"
                    + "INNER JOIN theatre ON sessions.theatreid = theatre.id \n"
                    + "INNER JOIN branch ON sessions.branchid = branch.id\n"
                    + "INNER JOIN schedule_times ON schedule_times.id = sessions.schedid\n"
                    + "WHERE 1 = 1 "
                    + "AND theatre.seatcapacity - (SELECT IFNULL(sum(seatcount),0) FROM bookings WHERE bookings.session_id = sessions.id AND status = 'CONFIRMED') <> 0 ";
            if (theatreId != 0) {
                selectStatement += "AND theatre.id = " + theatreId;
            }
            if (movieId != 0) {
                selectStatement += "AND movieid = " + movieId;
            }

            if (categoryId != 0) {
                selectStatement += "AND movies.cid= " + categoryId;
            }

            selectStatement += " GROUP BY sessions.id ";

            statement = connect.createStatement();
            resultSet = statement.executeQuery(selectStatement);

            while (resultSet.next()) {
                Vector v = new Vector();
                v.add(resultSet.getInt(1));
                v.add(resultSet.getString(2));
                v.add(resultSet.getString(3));
                v.add(resultSet.getString(4));
                v.add(resultSet.getString(5));
                v.add(resultSet.getString(6));
                v.add(resultSet.getString(7));

                resultV.add(v);
            }
            return resultV;

        } catch (SQLException e) {
            Utilities.conOut(e.getMessage());
        }
        return null;
    }
    
    public static String insertSession(int branchid, int theatreid,int movieid, Date from_date, Date to_date,int schedid){
        try
        {
            connectDB();
            preparedStatement = 
                    connect.prepareStatement
                            ("INSERT INTO `sessions`\n" +
                            "`branchid`,\n" +
                            "`theatreid`,\n" +
                            "`movieid`,\n" +
                            "`from_date`,\n" +
                            "`to_date`,\n" +
                            "`schedid`\n" +
                            ")\n" +
                            "VALUES\n" +
                            "(\n" +
                            "?,\n" +
                            "?,\n" +
                            "?,\n" +
                            "?,\n" +
                            "?,\n" +
                            "?\n" +
                            ");");
            preparedStatement.setInt(1, branchid);
            preparedStatement.setInt(2, theatreid);
            preparedStatement.setInt(3, movieid);
            preparedStatement.setDate(4,from_date);
            preparedStatement.setDate(5,to_date);
            preparedStatement.setInt(6, schedid);
            
            int checkInsert = preparedStatement.executeUpdate();
            if(checkInsert == 1){
                return "Session inserted successfully!";
            }
            
        }catch(SQLException e){
            e.printStackTrace();
        }
        return "";
    }

    public static String updateSession(int ID, int branchid, int theatreid,int movieid, Date from_date, Date to_date,int schedid){
        try
        {
            connectDB();
            preparedStatement = 
                    connect.prepareStatement
                            ("UPDATE `sessions`\n" +
                            "SET\n" +
                            "`branchid` = ?,\n" +
                            "`theatreid` = ?,\n" +
                            "`movieid` = ?,\n" +
                            "`from_date` = ?,\n" +
                            "`to_date` = ?,\n" +
                            "`schedid` = ?\n" +
                            "WHERE `id` = ?;");
            preparedStatement.setInt(1, branchid);
            preparedStatement.setInt(2, theatreid);
            preparedStatement.setInt(3, movieid);
            preparedStatement.setDate(4,from_date);
            preparedStatement.setDate(5,to_date);
            preparedStatement.setInt(6, schedid);
            preparedStatement.setInt(7, ID);
            
            int checkInsert = preparedStatement.executeUpdate();
            if(checkInsert == 1){
                return "Session inserted successfully!";
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return "";
    }
    
    public static String deleteSession(int ID){
        try{
            connectDB();
            
            preparedStatement = connect.prepareStatement("DELETE FROM sessions WHERE id = ?");
            preparedStatement.setInt(1, ID);
            
            int checkDelete = preparedStatement.executeUpdate();
            if(checkDelete == 1)
            {
                return "Record deleted successfully!";
            }
            
        }catch(SQLException e){
            e.printStackTrace();
        }
        return "";
    }
    //@Contract(pure = true)
    public static String makeBooking(int uid, int sessionId, int personCount) {

        try {
            connectDB();
            String execQ = "SELECT \n"
                    + "seatcapacity - (SELECT IFNULL(sum(seatcount),0) "
                    + "FROM bookings WHERE bookings.session_id = " + sessionId + " AND status = 'CONFIRMED') AS availableSeats\n"
                    + "FROM theatre WHERE id IN(SELECT sessions.theatreid \n"
                    + "FROM sessions \n"
                    + "INNER JOIN theatre \n"
                    + "ON sessions.theatreid = theatre.id WHERE sessions.id = " + sessionId + ") ;";
            statement = connect.createStatement();
            resultSet = statement.executeQuery(execQ);
            while (resultSet.next()) {
                int availableSeats = resultSet.getInt(1);

                if (availableSeats < personCount) {
                    //return "Are you booking for the whole city?";
                    return "You are expecting too much! Pick a humanely number of seats!";
                }
            }

            String execString = "INSERT INTO bookings (uid, session_id, seatcount) VALUES(?,?,?)";
            preparedStatement = connect.prepareStatement(execString);
            preparedStatement.setInt(1, uid);
            preparedStatement.setInt(2, sessionId);
            preparedStatement.setInt(3, personCount);

            if (preparedStatement.executeUpdate() == 1) {
                connect.close();
                return "Your booking is done!";
            } else {
                return "The seat count exceeds capacity!";
            }

            /*Checking if there is capacity for the sessions*/
        } catch (SQLException e) {
            Utilities.conOut(e.getMessage() + "@ making booking");
            return "";
        }
    }

    // @Nullable
    /*Lists bookings
    * @Param int uid = if(0){Returns all the bookings} else {returns user's}
    * */
    public static Vector listBookings(int uid, String status) {
        try {

            connectDB();

            Vector resultV = new Vector();

            //todo reassess the returning values.
            String selectStatement = "SELECT \n"
                    + "bookings.id, \n"
                    + "movie_name, theatre.theatrename, branch.branchname,\n"
                    + "schedule_times.start,\n"
                    + "schedule_times.end,\n"
                    + "seatcount,\n"
                    + "bookings.status,\n"
                    + "users.username \n"
                    + "  FROM bookings\n"
                    + "INNER JOIN sessions ON sessions.id = bookings.session_id\n"
                    + "INNER JOIN theatre ON theatre.id = sessions.theatreid\n"
                    + "INNER JOIN branch ON branch.id = sessions.branchid\n"
                    + "INNER JOIN movies ON movies.id = sessions.movieid\n"
                    + "INNER JOIN schedule_times ON sessions.schedid = schedule_times.id\n"
                    + "INNER JOIN users ON users.uid = bookings.uid\n"
                    + "WHERE 1 = 1 ";
            if (uid != 0) {
                selectStatement += " AND bookings.uid = " + uid;
            }
            if (!status.equals("")) {
                selectStatement += " AND status = " + status;
            }

            statement = connect.createStatement();
            resultSet = statement.executeQuery(selectStatement);

            while (resultSet.next()) {
                Vector v = new Vector();
                v.add(resultSet.getInt(1));
                v.add(resultSet.getString(2));
                v.add(resultSet.getString(3));
                v.add(resultSet.getString(4));
                v.add(resultSet.getString(5));
                v.add(resultSet.getString(6));
                v.add(resultSet.getInt(7));
                v.add(resultSet.getString(8));
                v.add(resultSet.getString(9));

                resultV.add(v);
            }

            return resultV;

        } catch (SQLException e) {
            Utilities.conOut(e.getMessage());
        }
        return null;
    }

    /*Should be called when register request is inbound
     * @Param string username
     * @Param string password | gets MD5 hashed internally
     * @Param int role ID | possible values admin = 1, user 2
     * @Param String email = user email as string
     * @Param string address = can be as long as possible
     * @Param string age = yes we require it as string to eliminate unnecessary parsing
     * @Param string gender = any value you wish to send :)
     * @Param isadmin = send true if willing to create an admin user
     */
    //  @NotNull
    
    public static String updateBooking(int bookingID, String status, boolean isAdmin) {
        try {
            connectDB();
            //TODO consider if a quota for a session will be held somewhere for increasing and decreasing after making a booking or cancelling a booking.
            //statement = connect.createStatement();
            //ResultSet selectedBooking = statement.executeQuery("SELECT * FROM bookings WHERE id = " + bookingID);

            String statementString = "UPDATE bookings SET status = ? WHERE id = ? ";

            if (!isAdmin) {
                statementString += " AND booking_time > DATE_SUB(NOW(), INTERVAL 1 DAY) ";

            }
            preparedStatement = connect.prepareStatement(statementString);
            preparedStatement.setString(1, status);
            preparedStatement.setInt(2, bookingID);

            int n = preparedStatement.executeUpdate();
            if (n == 0) {
                //this happens if the deletebooking request can not be carried out because there is less than 24 hours for the session
                return "Booking request can not be carried out because there is less than 24 hours for the session";
            }
            return "Booking updated succesfully!";

        } catch (SQLException e) {
            Utilities.conOut(e.getMessage());
            return "";
        }
    }
    
    public static String register(String username, String name, String surname, String password, int rid, String email, String address, String age, String gender, boolean isAdmin) {
        connectDB();
        try {
            String execSQL;
            if (isAdmin) {
                execSQL = "INSERT INTO `users`(`username`,`name`,`surname`,`password`,`rid`,`email`,`adress`,`age`,`gender`,`status`)VALUES(?,?,?,?,?,?,?,?,?,'active');";
            } else {
                execSQL = "INSERT INTO `users`(`username`,`name`,`surname`,`password`,`rid`,`email`,`adress`,`age`,`gender`)VALUES(?,?,?,?,?,?,?,?,?);";
            }
            preparedStatement = connect.prepareStatement(execSQL, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, name);
            preparedStatement.setString(3, surname);
            preparedStatement.setString(4, password);
            preparedStatement.setInt(5, rid);
            preparedStatement.setString(6, email);
            preparedStatement.setString(7, address);
            preparedStatement.setString(8, age);
            preparedStatement.setString(9, gender);
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            int uid = 0;

            while (resultSet.next()) {
                //getting last insert id
                uid = resultSet.getInt(1);
                Utilities.conOut("" + uid);
                break;
            }
            if (!isAdmin) {
                String token = Utilities.tokeniser("" + uid);
                insertToken(token, uid, "register");

                Utilities.conOut("User registered successfully");
                return token + "-" + email + "-" + uid; //TODO returning string is to be split by delim '-' for e-mailing the token to the user for activation.
            }
            return "adminInsert";
        } catch (SQLException e) {
            Utilities.conOut(e.getMessage());
            return "";
        }
    }

    public static Vector listCategories() {

        try {
            Vector resultV = new Vector();
            connectDB();
            statement = connect.createStatement();
            String execQ = "SELECT category_name FROM categories ORDER BY id ASC ";
            resultSet = statement.executeQuery(execQ);
                
            while (resultSet.next()) {
                resultV.add(resultSet.getString(1));
            }
            connect.close();
            return resultV;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return null;
    }

    public static Vector listTheatres() {
        try {
            connectDB();
            statement = connect.createStatement();
            resultSet = statement.executeQuery("SELECT `theatrename` FROM `theatre` ORDER BY id ASC ;");
            Vector resultV = new Vector();
            while (resultSet.next()) {
                Vector v = new Vector();
                v.add(resultSet.getString(1));
                resultV.add(v);
            }
            Utilities.conOut("Listing theatres successfully");
            connect.close();
            return resultV;
        } catch (SQLException e) {
            Utilities.conOut(e.getMessage());
            return null;
        }
    }

    public static Vector listBranches() {
        try {
            connectDB();
            statement = connect.createStatement();
            resultSet = statement.executeQuery("SELECT `branchname` FROM `branch` ORDER BY id ASC ;");
            Vector resultV = new Vector();
            while (resultSet.next()) {
                Vector v = new Vector();
                v.add(resultSet.getString(1));
                resultV.add(v);
            }
            Utilities.conOut("Listing branches successfully");
            connect.close();
            return resultV;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
    public static Vector scheduleTimes() {
        try {
            connectDB();
            statement = connect.createStatement();
            resultSet = statement.executeQuery("SELECT CONCAT(start , '-', end) FROM schedule_times ORDER BY id ASC ;");
            Vector resultV = new Vector();
            while (resultSet.next()) {
                Vector v = new Vector();
                v.add(resultSet.getString(1));
                resultV.add(v);
            }
            Utilities.conOut("Listing schedule times successfully");
            connect.close();
            return resultV;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
}
