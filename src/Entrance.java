
import static java.lang.System.exit;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public class Entrance extends javax.swing.JFrame {

    int signinClicks = 0;
    int rid = 0;
    String refMode = "";
    Vector movies = new Vector();
    Vector columnNames = new Vector();
    Enumeration en;
    DefaultTableModel model;
    private int uid;

    public Entrance() {
        initComponents();
        tablePanel.setVisible(false);
        makeBookingBtn.setVisible(false);
        Users.setVisible(false);
        Update.setVisible(false);

        Vector categories = DB.listCategories();
        Enumeration en;
        en = categories.elements();

        while (en.hasMoreElements()) {
            categoryComboBox.addItem(en.nextElement().toString());
        }

        columnNames = new Vector();
        columnNames.add("ID");
        columnNames.add("Branch Name");
        columnNames.add("Theatre Name");
        columnNames.add("Movie Name");
        columnNames.add("Session Hour");
        columnNames.add("Capacity");
        columnNames.add("Description");

    }

    private void listSessions(int index) {
        DefaultTableModel dtm = (DefaultTableModel) table.getModel();
        dtm.setRowCount(0);
        movies = DB.listAvailableSessions(0, 0, index + 1);
        model = new DefaultTableModel(columnNames, 0);
        Enumeration en = movies.elements();
        while (en.hasMoreElements()) {
            model.addRow((Vector) en.nextElement());
            //Utilities.conOut("" + en.nextElement());
        }
        table.setModel(model);
        //table.setAutoResizeMode(table.AUTO_RESIZE_OFF);

    }

    private void showAllUsers() {
        Vector users = new Vector();
        DefaultTableModel dtm = (DefaultTableModel) table.getModel();
        dtm.setRowCount(0);
        users = DB.listUsers(0);
        Vector cNames = new Vector();
        cNames.add("ID");
        cNames.add("Name");
        cNames.add("Surname");
        cNames.add("Username");
        cNames.add("E-Mail");
        cNames.add("Address");
        cNames.add("Age");
        cNames.add("Gender");
        cNames.add("Status");

        model = new DefaultTableModel(cNames, 0);
        Enumeration en = users.elements();
        while (en.hasMoreElements()) {
            model.addRow((Vector) en.nextElement());
            //Utilities.conOut("" + en.nextElement());
        }
        table.setModel(model);
    }

    private void listBookings(int uid) {
        DefaultTableModel dtm = (DefaultTableModel) table.getModel();
        dtm.setRowCount(0);
        movies = DB.listBookings(uid, "");
        Vector cNames = new Vector();
        cNames.add("ID");
        cNames.add("Movie Name");
        cNames.add("Theatre");
        cNames.add("Branch");
        cNames.add("Start");
        cNames.add("End");
        cNames.add("Seats");
        cNames.add("Status");
        cNames.add("Username");
        model = new DefaultTableModel(cNames, 0);
        Enumeration en = movies.elements();
        while (en.hasMoreElements()) {
            model.addRow((Vector) en.nextElement());
            //Utilities.conOut("" + en.nextElement());
        }
        table.setModel(model);
        //table.setAutoResizeMode(table.AUTO_RESIZE_OFF);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        signInbtn = new javax.swing.JButton();
        userNametxt = new javax.swing.JTextField();
        usernameLBL = new javax.swing.JLabel();
        passwordLBL = new javax.swing.JLabel();
        passwordtxt = new javax.swing.JPasswordField();
        scrollPane = new javax.swing.JScrollPane();
        tablePanel = new javax.swing.JPanel();
        categoryComboBox = new javax.swing.JComboBox<>();
        label = new javax.swing.JLabel();
        tableScrollPane = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        makeBookingBtn = new javax.swing.JButton();
        cancelBookingBtn = new javax.swing.JButton();
        Update = new javax.swing.JButton();
        refreshBTN = new javax.swing.JButton();
        blockBTN = new javax.swing.JButton();
        WelcomeLBL = new javax.swing.JLabel();
        signUpBTN = new javax.swing.JButton();
        menuNavigator = new javax.swing.JMenuBar();
        menu = new javax.swing.JMenu();
        menu_signUp = new javax.swing.JMenuItem();
        menu_exit = new javax.swing.JMenuItem();
        view = new javax.swing.JMenu();
        viewMovies = new javax.swing.JMenuItem();
        viewBookings = new javax.swing.JMenuItem();
        Users = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Entrance");
        setName("entranceFrame"); // NOI18N

        signInbtn.setText("Sign In");
        signInbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                signInbtnActionPerformed(evt);
            }
        });

        userNametxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userNametxtActionPerformed(evt);
            }
        });

        usernameLBL.setText("Username");

        passwordLBL.setText("Password");

        scrollPane.setViewportBorder(javax.swing.BorderFactory.createTitledBorder("News and Ads"));

        categoryComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                categoryComboBoxActionPerformed(evt);
            }
        });

        label.setText("Select Category:");

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tableScrollPane.setViewportView(table);

        makeBookingBtn.setText("Book");
        makeBookingBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                makeBookingBtnActionPerformed(evt);
            }
        });

        cancelBookingBtn.setText("Cancel");
        cancelBookingBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBookingBtnActionPerformed(evt);
            }
        });

        Update.setText("Update");
        Update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UpdateActionPerformed(evt);
            }
        });

        refreshBTN.setText("Refresh");
        refreshBTN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshBTNActionPerformed(evt);
            }
        });

        blockBTN.setText("Block");
        blockBTN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blockBTNActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout tablePanelLayout = new javax.swing.GroupLayout(tablePanel);
        tablePanel.setLayout(tablePanelLayout);
        tablePanelLayout.setHorizontalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tablePanelLayout.createSequentialGroup()
                .addComponent(tableScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 873, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 929, Short.MAX_VALUE))
            .addGroup(tablePanelLayout.createSequentialGroup()
                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tablePanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(categoryComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(279, 279, 279)
                        .addComponent(refreshBTN))
                    .addGroup(tablePanelLayout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(makeBookingBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(159, 159, 159)
                        .addComponent(cancelBookingBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(Update, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(blockBTN, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(1092, Short.MAX_VALUE))
        );
        tablePanelLayout.setVerticalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tablePanelLayout.createSequentialGroup()
                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tablePanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(label)
                            .addComponent(categoryComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(refreshBTN))
                .addGap(18, 18, 18)
                .addComponent(tableScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(makeBookingBtn)
                    .addComponent(cancelBookingBtn)
                    .addComponent(Update)
                    .addComponent(blockBTN))
                .addContainerGap(798, Short.MAX_VALUE))
        );

        scrollPane.setViewportView(tablePanel);

        WelcomeLBL.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        WelcomeLBL.setText("WELCOME");

        signUpBTN.setText("Sign Up");
        signUpBTN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                signUpBTNActionPerformed(evt);
            }
        });

        menu.setText("Menu");

        menu_signUp.setText("Sign Up");
        menu_signUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_signUpActionPerformed(evt);
            }
        });
        menu.add(menu_signUp);

        menu_exit.setText("Exit");
        menu_exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_exitActionPerformed(evt);
            }
        });
        menu.add(menu_exit);

        menuNavigator.add(menu);

        view.setText("View");

        viewMovies.setText("Movies");
        viewMovies.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewMoviesActionPerformed(evt);
            }
        });
        view.add(viewMovies);

        viewBookings.setText("My Bookings");
        viewBookings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewBookingsActionPerformed(evt);
            }
        });
        view.add(viewBookings);

        Users.setText("Users");
        Users.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsersActionPerformed(evt);
            }
        });
        view.add(Users);

        menuNavigator.add(view);

        setJMenuBar(menuNavigator);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 975, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(WelcomeLBL, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(signUpBTN, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(signInbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(usernameLBL, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(userNametxt, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(passwordLBL, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(passwordtxt, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(userNametxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(usernameLBL)
                            .addComponent(passwordLBL)
                            .addComponent(passwordtxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(signInbtn))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(WelcomeLBL, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                        .addComponent(signUpBTN)))
                .addGap(13, 13, 13)
                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 443, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void signInbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signInbtnActionPerformed

        if (userNametxt.getText().equals("")) {
            signinClicks++;
        }
        if (signinClicks > 10) {
            //TODO decide what to do down here
            Utilities.promptOut(null, "Easter egg mofo");
        }

        String usrnm = userNametxt.getText();
        String passwd = Utilities.MD5Hash(passwordtxt.getText());
        Utilities.conOut(passwd);
        try {
            String checkLoginString = DB.checkLogin(usrnm, passwd);
            Utilities.conOut(checkLoginString);
            String[] parts = checkLoginString.split("-");
            rid = Integer.parseInt(parts[0]);
            uid = Integer.parseInt(parts[1]);
            if (rid != 0) {
                usernameLBL.setVisible(false);
                userNametxt.setVisible(false);
                passwordLBL.setVisible(false);
                passwordtxt.setVisible(false);
                signUpBTN.setVisible(false);
                signInbtn.setVisible(false);
                makeBookingBtn.setVisible(true);
                //these are for admin only
                if (rid == 1) {
                    Users.setVisible(true);
                    Update.setVisible(true);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Entrance.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_signInbtnActionPerformed

    private void userNametxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userNametxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_userNametxtActionPerformed

    private void menu_signUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_signUpActionPerformed
        new SignUp(rid, false, null).setVisible(true);
    }//GEN-LAST:event_menu_signUpActionPerformed

    private void menu_exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_exitActionPerformed
        exit(0);
    }//GEN-LAST:event_menu_exitActionPerformed

    private void signUpBTNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signUpBTNActionPerformed
        new SignUp(rid, false, null).setVisible(true);
    }//GEN-LAST:event_signUpBTNActionPerformed

    private void viewMoviesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewMoviesActionPerformed
        refMode = "movies";
        this.categoryComboBox.setVisible(true);
        this.label.setVisible(true);
        tablePanel.setVisible(true);
        this.makeBookingBtn.setVisible(true);
        this.cancelBookingBtn.setVisible(false);
        this.Update.setVisible(false);
        this.blockBTN.setVisible(false);
        listSessions(categoryComboBox.getSelectedIndex() + 1);
        
    }//GEN-LAST:event_viewMoviesActionPerformed

    private void categoryComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_categoryComboBoxActionPerformed

        if (categoryComboBox.getSelectedIndex() != -1) {
            listSessions(categoryComboBox.getSelectedIndex());
        }
    }//GEN-LAST:event_categoryComboBoxActionPerformed

    private void viewBookingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewBookingsActionPerformed
        refMode = "bookings";
        this.categoryComboBox.setVisible(false);
        this.label.setVisible(false);
        this.tablePanel.setVisible(true);
        this.listBookings(uid);
        makeBookingBtn.setVisible(false);
        cancelBookingBtn.setVisible(true);
        Update.setVisible(false);
        blockBTN.setVisible(false);
    }//GEN-LAST:event_viewBookingsActionPerformed

    private void makeBookingBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_makeBookingBtnActionPerformed
        if (table.getSelectedRowCount() == 0) {
            Utilities.promptOut(null, "Select the session you want to book!");
        } else {
            try {
                int seatCount = Integer.parseInt(Utilities.promtIn(null, "How many seats?"));
                int row = table.getSelectedRow();
                int ID = 0;
                ID = (int) table.getModel().getValueAt(row, 0);
                boolean confirmBooking = Utilities.getConfirmation("Are you sure you want to make this booking?", null);
                if (confirmBooking) {
                    Utilities.promptOut(null, DB.makeBooking(uid, ID, seatCount));
                    //TODO send the booking details to the MAFAKIN via e-mail
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }//GEN-LAST:event_makeBookingBtnActionPerformed

    private void cancelBookingBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBookingBtnActionPerformed
        if (table.getSelectedRowCount() == 0) {
            Utilities.promptOut(null, "Select the session you want to book!");
        } else {
            try {
                int row = table.getSelectedRow();
                int ID = 0;
                ID = (int) table.getModel().getValueAt(row, 0);
                boolean confirmBooking = Utilities.getConfirmation("Are you sure you want to cancel this booking?", null);
                if (confirmBooking) {
                    Utilities.promptOut(null, DB.updateBooking(ID, "CANCELLED", false));
                    //TODO send the booking details to the MAFAKIN via e-mail
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_cancelBookingBtnActionPerformed

    private void UsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsersActionPerformed
        refMode = "users";
        tablePanel.setVisible(true);
        categoryComboBox.setVisible(false);
        label.setVisible(false);
        cancelBookingBtn.setVisible(false);
        makeBookingBtn.setVisible(false);
        Update.setVisible(true);
        blockBTN.setVisible(true);

        showAllUsers();
    }//GEN-LAST:event_UsersActionPerformed

    private void UpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UpdateActionPerformed

        if (table.getSelectedRowCount() == 0) {
            Utilities.promptOut(null, "Select the user you want to update!");
        } else {
            try {
                int row = table.getSelectedRow();
                int ID = 0;
                ID = (int) table.getModel().getValueAt(row, 0);
                Vector<String> rowData = new Vector<String>();

                rowData.add("" + table.getModel().getValueAt(row, 0));
                rowData.add("" + table.getModel().getValueAt(row, 1));
                rowData.add("" + table.getModel().getValueAt(row, 2));
                rowData.add("" + table.getModel().getValueAt(row, 3));
                rowData.add("" + table.getModel().getValueAt(row, 4));
                rowData.add("" + table.getModel().getValueAt(row, 5));
                rowData.add("" + table.getModel().getValueAt(row, 6));
                rowData.add("" + table.getModel().getValueAt(row, 7));
                rowData.add("" + table.getModel().getValueAt(row, 8));
                SignUp su = new SignUp(1, true, rowData);
                su.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_UpdateActionPerformed

    private void refreshBTNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshBTNActionPerformed
        if (refMode.equals("movies")) {
            this.categoryComboBox.setVisible(true);
            this.label.setVisible(true);
            listSessions(categoryComboBox.getSelectedIndex() + 1);
        }
        if (refMode.equals("users")) {
            this.categoryComboBox.setVisible(false);
            this.label.setVisible(false);
            showAllUsers();
        }
        if (refMode.equals("bookings")) {
            this.blockBTN.setVisible(false);
            this.Update.setVisible(false);
            if (this.rid == 1) {
                listBookings(0);
            } else {
                listBookings(uid);
            }

        }
    }//GEN-LAST:event_refreshBTNActionPerformed

    private void blockBTNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blockBTNActionPerformed
         if (table.getSelectedRowCount() == 0) {
            Utilities.promptOut(null, "Select the user you want to block!");
        } else {
            try {
                int row = table.getSelectedRow();
                int ID = 0;
                ID = (int) table.getModel().getValueAt(row, 0);
                if(DB.updateUserStatus(ID, "blocked")){
                    Utilities.promptOut(null, "The selected user is blocked forever, the MOFO has to sign up again!");
                }
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_blockBTNActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Entrance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Entrance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Entrance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Entrance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Entrance().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Update;
    private javax.swing.JMenuItem Users;
    private javax.swing.JLabel WelcomeLBL;
    private javax.swing.JButton blockBTN;
    private javax.swing.JButton cancelBookingBtn;
    private javax.swing.JComboBox<String> categoryComboBox;
    private javax.swing.JLabel label;
    private javax.swing.JButton makeBookingBtn;
    private javax.swing.JMenu menu;
    private javax.swing.JMenuBar menuNavigator;
    private javax.swing.JMenuItem menu_exit;
    private javax.swing.JMenuItem menu_signUp;
    private javax.swing.JLabel passwordLBL;
    private javax.swing.JPasswordField passwordtxt;
    private javax.swing.JButton refreshBTN;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JButton signInbtn;
    private javax.swing.JButton signUpBTN;
    private javax.swing.JTable table;
    private javax.swing.JPanel tablePanel;
    private javax.swing.JScrollPane tableScrollPane;
    private javax.swing.JTextField userNametxt;
    private javax.swing.JLabel usernameLBL;
    private javax.swing.JMenu view;
    private javax.swing.JMenuItem viewBookings;
    private javax.swing.JMenuItem viewMovies;
    // End of variables declaration//GEN-END:variables

}
