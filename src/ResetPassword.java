
public class ResetPassword extends javax.swing.JFrame {

    public ResetPassword() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        resetPassLBL = new javax.swing.JLabel();
        newPassLBL = new javax.swing.JLabel();
        passTXT = new javax.swing.JPasswordField();
        confirmPassLBL = new javax.swing.JLabel();
        retypePassTXT = new javax.swing.JPasswordField();
        resetPassBTN = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Forgot Password");

        resetPassLBL.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        resetPassLBL.setText("Reset Password");

        newPassLBL.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        newPassLBL.setText("New Password");

        passTXT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passTXTActionPerformed(evt);
            }
        });

        confirmPassLBL.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        confirmPassLBL.setText("Re-Type Password");

        retypePassTXT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retypePassTXTActionPerformed(evt);
            }
        });

        resetPassBTN.setText("Reset Password");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(newPassLBL)
                        .addGap(30, 30, 30)
                        .addComponent(passTXT, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(confirmPassLBL)
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(resetPassBTN, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(retypePassTXT, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(13, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(resetPassLBL)
                .addGap(154, 154, 154))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(resetPassLBL)
                .addGap(52, 52, 52)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(newPassLBL)
                    .addComponent(passTXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(confirmPassLBL, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(retypePassTXT, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                .addComponent(resetPassBTN, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void passTXTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passTXTActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_passTXTActionPerformed

    private void retypePassTXTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_retypePassTXTActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_retypePassTXTActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel confirmPassLBL;
    private javax.swing.JLabel newPassLBL;
    private javax.swing.JPasswordField passTXT;
    private javax.swing.JButton resetPassBTN;
    private javax.swing.JLabel resetPassLBL;
    private javax.swing.JPasswordField retypePassTXT;
    // End of variables declaration//GEN-END:variables
}
