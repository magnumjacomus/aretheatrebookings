
import java.awt.Color;
import java.io.IOException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SignUp extends javax.swing.JFrame {

    private int upId = 0;
    private boolean updateMode;

    public SignUp(int rid, boolean uMode, Vector params) {
        initComponents();
        updateMode = uMode;
        if (updateMode == true) {
            this.maleRadio.setSelected(false);
            this.femaleRadio.setSelected(false);
            this.setTitle("UPDATE USER");
            // this.signUpBTN.setText("");
            this.signUpBTN.setText("UPDATE");
            this.signUpLBL.setText("Update User");

            upId = Integer.parseInt(params.get(0).toString());
            nameTXT.setText(""+params.get(1));
            surnameTXT.setText(""+params.get(2));
            usernameTXT.setText("" + params.get(3).toString());
            emailTXT.setText("" + params.get(4));
            addressTXT.setText("" + params.get(5));
            ageTXT.setText("" + params.get(6));
            
            String gender = params.get(7).toString();
            
            if(gender.equals("female")){
                this.femaleRadio.setSelected(true);
            }
            else{
                this.maleRadio.setSelected(true);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        genderBtnGrp = new javax.swing.ButtonGroup();
        nameLBL = new javax.swing.JLabel();
        surnameLBL = new javax.swing.JLabel();
        emailLBL = new javax.swing.JLabel();
        ageLBL = new javax.swing.JLabel();
        passwordLBL = new javax.swing.JLabel();
        reTypePasswordLBL = new javax.swing.JLabel();
        usernameLBL = new javax.swing.JLabel();
        retypePassTXT = new javax.swing.JPasswordField();
        passwordTXT = new javax.swing.JPasswordField();
        nameTXT = new javax.swing.JTextField();
        surnameTXT = new javax.swing.JTextField();
        emailTXT = new javax.swing.JTextField();
        usernameTXT = new javax.swing.JTextField();
        signUpBTN = new javax.swing.JButton();
        addressLBL = new javax.swing.JLabel();
        addressTXT = new javax.swing.JTextField();
        genderLBL = new javax.swing.JLabel();
        maleRadio = new javax.swing.JRadioButton();
        femaleRadio = new javax.swing.JRadioButton();
        ageTXT = new javax.swing.JTextField();
        signUpLBL = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Sign Up");

        nameLBL.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        nameLBL.setText("Name");

        surnameLBL.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        surnameLBL.setText("Surname");

        emailLBL.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        emailLBL.setText("E-Mail");

        ageLBL.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        ageLBL.setText("Age");

        passwordLBL.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        passwordLBL.setText("Password");

        reTypePasswordLBL.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        reTypePasswordLBL.setText("Re-Type Password");

        usernameLBL.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        usernameLBL.setText("Username");

        signUpBTN.setText("Sign Up");
        signUpBTN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                signUpBTNActionPerformed(evt);
            }
        });

        addressLBL.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        addressLBL.setText("Address");

        genderLBL.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        genderLBL.setText("Gender");

        genderBtnGrp.add(maleRadio);
        maleRadio.setText("Male");

        genderBtnGrp.add(femaleRadio);
        femaleRadio.setText("Female");

        signUpLBL.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        signUpLBL.setText("Sign Up");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(reTypePasswordLBL)
                            .addComponent(passwordLBL)
                            .addComponent(usernameLBL)
                            .addComponent(nameLBL)
                            .addComponent(surnameLBL)
                            .addComponent(emailLBL)
                            .addComponent(ageLBL)
                            .addComponent(addressLBL)
                            .addComponent(genderLBL))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(nameTXT)
                            .addComponent(passwordTXT, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
                            .addComponent(retypePassTXT)
                            .addComponent(surnameTXT)
                            .addComponent(emailTXT)
                            .addComponent(usernameTXT)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(118, 118, 118)
                                .addComponent(signUpBTN, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(addressTXT)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(maleRadio)
                                .addGap(18, 18, 18)
                                .addComponent(femaleRadio))
                            .addComponent(ageTXT)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(453, 453, 453)
                        .addComponent(signUpLBL)))
                .addContainerGap(159, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(signUpLBL)
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nameLBL)
                    .addComponent(nameTXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(surnameLBL)
                    .addComponent(surnameTXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(emailLBL)
                    .addComponent(emailTXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(usernameLBL)
                    .addComponent(usernameTXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(passwordLBL)
                    .addComponent(passwordTXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(reTypePasswordLBL)
                    .addComponent(retypePassTXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(ageLBL)
                    .addComponent(ageTXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addressLBL)
                    .addComponent(addressTXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(genderLBL)
                    .addComponent(maleRadio)
                    .addComponent(femaleRadio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(signUpBTN, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(86, 86, 86))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void signUpBTNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signUpBTNActionPerformed
        String usrnm = usernameTXT.getText();
        String name = nameTXT.getText();
        String surname = surnameTXT.getText();
        String passwd = Utilities.MD5Hash(passwordTXT.getText());
        String email = emailTXT.getText();
        String address = addressTXT.getText();
        String age = ageTXT.getText();
        String gender = "";
        if (maleRadio.isSelected()) {
            gender = "male";
        }
        if (femaleRadio.isSelected()) {
            gender = "female";
        }

        //if (passwordTXT.getText().toString() != retypePassTXT.getText()){
        if (!passwordTXT.getText().equals(retypePassTXT.getText())) {
            Utilities.promptOut(null, "Passwords do not match!");
            retypePassTXT.setBackground(Color.RED);
            return;
        }
        if (this.updateMode == false) {
            try {
                if (Utilities.isInternetAvailable()) {
                    String ret = DB.register(usrnm, name, surname, passwd, 2, email, address, age, gender, false);
                    String[] parts = ret.split("-");
                    String token = parts[0];
                    String retEmail = parts[1];
                    int uid = Integer.parseInt(parts[2]);
                    String subj = "Register confirmation";
                    String body = "Please provide the following token within the application's input dialog!\nToken: " + token;
                    if (Mailer.sendTls(retEmail, subj, body)) {
                        String inputToken = Utilities.promtIn(null, "Please paste your token here and hit ok!");
                        Utilities.promptOut(null, DB.checkToken(token, "register", uid));
                        this.dispose();
                    }
                }
            } catch (IOException ex) {
                Utilities.promptOut(null, "No internet connection!"
                        + "Please note that it is necessary to have a valid internet connection to complete sign up process!");
                Logger.getLogger(SignUp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            if(Utilities.getConfirmation("Update user?", null)){
                Utilities.promptOut(null, DB.updateUser(upId, usrnm, name,surname, passwd, email, address, Integer.parseInt(age), gender, "active"));
                this.dispose();
                
            }
        }
    }//GEN-LAST:event_signUpBTNActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel addressLBL;
    private javax.swing.JTextField addressTXT;
    private javax.swing.JLabel ageLBL;
    private javax.swing.JTextField ageTXT;
    private javax.swing.JLabel emailLBL;
    private javax.swing.JTextField emailTXT;
    private javax.swing.JRadioButton femaleRadio;
    private javax.swing.ButtonGroup genderBtnGrp;
    private javax.swing.JLabel genderLBL;
    private javax.swing.JRadioButton maleRadio;
    private javax.swing.JLabel nameLBL;
    private javax.swing.JTextField nameTXT;
    private javax.swing.JLabel passwordLBL;
    private javax.swing.JPasswordField passwordTXT;
    private javax.swing.JLabel reTypePasswordLBL;
    private javax.swing.JPasswordField retypePassTXT;
    private javax.swing.JButton signUpBTN;
    private javax.swing.JLabel signUpLBL;
    private javax.swing.JLabel surnameLBL;
    private javax.swing.JTextField surnameTXT;
    private javax.swing.JLabel usernameLBL;
    private javax.swing.JTextField usernameTXT;
    // End of variables declaration//GEN-END:variables
}
