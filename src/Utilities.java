import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Utilities {
    private static DB db = new DB();

    public static void conOut(String message) {
        System.out.println(message);
    }

    public static boolean getConfirmation(String message, Component component) {
        int x = JOptionPane.showConfirmDialog(component, message);
        if (x == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static void promptOut(Component c, String message){
        JOptionPane.showMessageDialog(c,message);
    }
    
    public static String promtIn(Component c, String message){
        return JOptionPane.showInputDialog(c,message);
    }
    

    /*
    * This is being used for creating user token for pass reset and registration
    * @Param uid = unique user id
    * */
    public static String tokeniser(String uid) {
        String token = MD5Hash(uid + "" + System.currentTimeMillis());
        return token;
    }

    public static String MD5Hash(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (byte anArray : array) {
                sb.append(Integer.toHexString((anArray & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static boolean isInternetAvailable() throws IOException
    {
        return isHostAvailable("google.com") || isHostAvailable("amazon.com")
                || isHostAvailable("facebook.com")|| isHostAvailable("apple.com");
    }

    private static boolean isHostAvailable(String hostName) throws IOException
    {
        try(Socket socket = new Socket())
        {
            int port = 80;
            InetSocketAddress socketAddress = new InetSocketAddress(hostName, port);
            socket.connect(socketAddress, 3000);

            return true;
        }
        catch(UnknownHostException unknownHost)
        {
            return false;
        }
    }
}
